/*
 EJERCICIO:

Dado N números enteros positivos, encuentre la suma mínima y máxima de los valores que puedan ser calculados sumando
N-1 de los N números. Imprímalos en pantalla.

Ejemplo: N = 5, se calcula la suma mínima y máxima tomando de a 4 elementos.

Arreglo: 2 4 1 3 5

Mínimo 10

Máximo 14

Explicación:
Si sumamos todos excepto 2, tenemos 4 + 1 + 3 + 5 = 13
Si sumamos todos excepto 4, tenemos 2 + 1 + 3 + 5 = 11
Si sumamos todos excepto 1, tenemos 2 + 4 + 3 + 5 = 14
Si sumamos todos excepto 3, tenemos 2 + 4 + 1 + 5 = 12
Si sumamos todos excepto 5, tenemos 2 + 4 + 1 + 3 = 10
 */
package codigo;

import java.util.Scanner;

/**
 *
 * @author yerssonyrc
 */
public class Codigo {

    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
            System.out.println("digite la cantidad del arreglo");
            Scanner teclado = new Scanner(System.in);
             int n = 0,sumamayor=0,sumamenor=0,menor = 0,mayor = 0;
             n=teclado.nextInt();
             int a[]=new int[n];
        for (int i = 0; i < n; i++) {
             System.out.println("digite los valores de arreglo   -- "+i);
             a[i]=teclado.nextInt();  
        }
        System.out.println("los valores del arreglo son ");
        for (int i = 0; i < n ; i++){
           
            System.out.println(a[i]);   
        
                menor=a[0];
                mayor=a[0];
        for (int j = 1; j < n; j++) {
            if (a[j]>mayor) {
                mayor=a[j];
            }
            if (a[j]<menor) {
                menor=a[j];
            }
        }
        }
         for (int i = 0; i < n ; i++){//sobran lineas de codigo se puede resumimr 
          sumamayor=sumamayor+a[i];
          sumamenor=sumamenor+a[i];
        } 
         sumamayor+=-menor;
         sumamenor+=-mayor;
        System.out.println("numero mayor es :"+mayor);
        System.out.println("numero menor es :"+menor+"\n");
        System.out.println("la suma maxima :"+sumamayor);
        System.out.println("la suma minima :"+sumamenor);
    }
}

     
 